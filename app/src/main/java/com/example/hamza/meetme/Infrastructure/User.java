package com.example.hamza.meetme.Infrastructure;


import java.lang.ref.SoftReference;

public class User {

    private int id;
    private String userName;
    private String name;
    private String avatarURL;
    private Boolean isLoggedIN;
    private String hasPassword;
    private String email ;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatarURL() {
        return avatarURL;
    }

    public void setAvatarURL(String avatarURL) {
        this.avatarURL = avatarURL;
    }

    public Boolean getIsLoggedIN() {
        return isLoggedIN;
    }

    public void setIsLoggedIN(Boolean isLoggedIN) {
        this.isLoggedIN = isLoggedIN;
    }

    public String getHasPassword() {
        return hasPassword;
    }

    public void setHasPassword(String hasPassword) {
        this.hasPassword = hasPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
