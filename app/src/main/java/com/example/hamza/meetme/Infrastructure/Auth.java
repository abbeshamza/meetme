package com.example.hamza.meetme.Infrastructure;

import android.content.Context;

/**
 * Created by hamza on 31/01/2016.
 */
public class Auth {

    private final Context context ;
    private User user;

    public Auth(Context context) {
        this.context = context;
        user = new User();
    }

    public User getUser() {
        return user;
    }
}
