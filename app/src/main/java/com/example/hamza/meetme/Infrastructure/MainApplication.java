package com.example.hamza.meetme.Infrastructure;

import android.app.Application;

/**
 * Created by hamza on 31/01/2016.
 */
public class MainApplication extends Application {
    private  Auth auth;

    @Override
    public void onCreate() {
        super.onCreate();
       auth = new Auth(this);
        auth.getUser().setIsLoggedIN(false);
    }


    public Auth getAuth() {
        return auth;
    }


}
