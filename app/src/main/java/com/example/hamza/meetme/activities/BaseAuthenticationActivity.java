package com.example.hamza.meetme.activities;

import android.content.Intent;
import android.os.Bundle;

/**
 * Created by hamza on 31/01/2016.
 */
public abstract class BaseAuthenticationActivity extends BaseActivity {
    @Override
    protected final void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        if ( application.getAuth().getUser().getIsLoggedIN()== false)
        {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
            return;
        }
        onMeetmeCreate(savedInstanceState);
    }


    protected abstract void onMeetmeCreate(Bundle savedState);
}
