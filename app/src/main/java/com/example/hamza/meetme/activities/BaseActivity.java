package com.example.hamza.meetme.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;

import com.example.hamza.meetme.Infrastructure.MainApplication;

/**
 * Created by hamza on 31/01/2016.
 */
public abstract class BaseActivity extends ActionBarActivity {
    protected MainApplication application ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        application = (MainApplication) getApplication();
    }
}
