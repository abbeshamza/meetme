package com.example.hamza.meetme.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.hamza.meetme.R;

/**
 * Created by hamza on 31/01/2016.
 */
public class LoginActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public void doLogin(View view) {
        application.getAuth().getUser().setIsLoggedIN(true);
        startActivity(new Intent(this , MainActivity.class));
        finish();
    }
}
